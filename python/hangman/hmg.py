from turtle import *
import turtle
import time 


def hangman(count):
    penup()
    goto(-100,100)
    pendown()
    size =60
    P =[1,2,3,4,5,6]
    if count in P:
        turtle.goto(-100,100)
        fd(size*2)
        right(90)
        fd(size*3)
        left(90)
        P.pop(0)
#head
        circle(size)
        if count in P:
#arms
            fd(size)
            P.pop(0)
            if count in P:            
                bk(size*2)
                fd(size)
                P.pop(0)
                if count in P:
#body
                    right(90)
                    fd(size*1.5)
                    P.pop(0)
                    if count in P:
#legs
                        right(45)
                        fd(size)
                        bk(size)
                        P.pop(0)
                        if count in P:
                            left(90)
                            fd(size)
                            hideturtle()
                            time.sleep(1)

    clear()
    return count + 1
count = 6
while count < 7:
    b = hangman(count)
    count = b
