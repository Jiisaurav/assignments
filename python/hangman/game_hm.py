from turtle import *
import turtle
import time

def hangman(count):
    penup()
    goto(-100,100)
    pendown()
    size =60
    size =60
    P =[1,2,3,4,5,6]
    if count in P:
        fd(size*2)
        right(90)
        fd(size*3)
        left(90)
        P.pop(0)
#head
        circle(size)
        if count in P:
#arms
            fd(size)
            P.pop(0)
            if count in P:            
                bk(size*2)
                fd(size)
                P.pop(0)
                if count in P:
#body
                    right(90)
                    fd(size*1.5)
                    P.pop(0)
                    if count in P:
#legs
                        right(45)
                        fd(size)
                        bk(size)
                        P.pop(0)
                        if count in P:
                            left(90)
                            fd(size)
                            hideturtle()
                            time.sleep(1)
    clear()
    return count + 1

def animals():
    print ('Enter the number to select the question: ')
    animal = {
        1 : 'LION', 2 : 'ELEPHANT' , 3: 'HIPPOTAMOUS' , 4: 'RHINO'
    }
    print ("1. Which animal is the King of the Jungle? \n2. Which animal is the largest animal? \n3. Largest animal that lives in river? \n4. Animal With Toughest Skin?")
    sel = int(input('___'))
    if sel in [1,2,3,4]:
        return animal.get(sel)
    else: 
        print("select the given question number only ")
        animals()

def plants():
    print ('Enter the number to select the question: ')
    animal = {
        1 : 'LION', 2 : 'ELEPHANT' , 3: 'HIPPOTAMOUS' , 4: 'RHINO'
    }
    print ("1. Which animal is the King of the Jungle? \n2. Which animal is the largest animal? \n3. Largest animal that lives in river? \n4. Animal With Toughest Skin?")
    sel = int(input('___'))
    if sel in [1,2,3,4]:
        return animal.get(sel)
    else: 
        print("select the given question number only ")
        plants()

def food():
    print ('Enter the number to select the question: ')
    animal = {
        1 : 'LION', 2 : 'ELEPHANT' , 3: 'HIPPOTAMOUS' , 4: 'RHINO'
    }
    print ("1. Which animal is the King of the Jungle? \n2. Which animal is the largest animal? \n3. Largest animal that lives in river? \n4. Animal With Toughest Skin?")
    sel = int(input('___'))
    if sel in [1,2,3,4]:
        return animal.get(sel)
    else: 
        print("select the given question number only ")
        food()

def capitals():
    print ('Enter the number to select the question: ')
    animal = {
        1 : 'LION', 2 : 'ELEPHANT' , 3: 'HIPPOTAMOUS' , 4: 'RHINO'
    }
    print ("1. Which animal is the King of the Jungle? \n2. Which animal is the largest animal? \n3. Largest animal that lives in river? \n4. Animal With Toughest Skin?")
    sel = int(input('___'))
    if sel in [1,2,3,4]:
        return animal.get(sel)
    else: 
        print("select the given question number only ")
        capitals()

def lgic(x):
    b= x.upper()
    vlu = list(b)
    ans = ['_ ' for h in b]
    count = 1
    rslt = ' '
    while count < 7:
        ip = input().upper()
        if ip in vlu:
            c = [indx for indx, i in enumerate(b) if i == ip]
            for j in c:
                ans[j] = ip
            print(rslt.join(ans))
            if ans == vlu:
                strt()
        else:
            cntr = hangman(count)
            count = cntr 

def strt():
    print ('_'*4 + '-'*2 +'WELCOME TO THE GAME' + '-'*2 + '_'*4  +' \n \n \n \n              ENJOY')
    print ('\nSELECT THE LEVEL U WANT TO PLAY: \n')
    print('CAPITALS ----> 1 \nANIMALS  ----> 2 \nFOOD     ----> 3 \nPLANTS   ----> 4')
    sel = int(input('___'))
    if sel == 1:
        x = capitals()
    elif sel == 2:
        x = animals()
    elif sel == 3:
        x = food()
    elif sel == 4:
        x = plants()
    
    print ('_ '*len(x) )
    lgic(x)
strt()