import re
class Time:
    def __init__(self, hour , minute, second ):
        self.hour = hour
        self.minute = minute
        self.second = second
        
    def adds (self,t2):
        min = 0
        hr = 0
        second = self.second + t2.second
        if second > 60:
            second -=  60
            min = 1
        minute = self.minute + t2.minute + min
        if minute > 60:
            minute -= 60
            hr = 1  
        hour = self.hour + t2.hour + hr
        return Time(hour,minute,second)
    
    def __str__(self) -> str:
        return f'{self.hour}:{self.minute}:{self.second}'
        pass

t1 = Time(2,50,60) # current time
d1 =Time(1,40,40) #duration of the movie

tltme = t1.adds(d1)
print (tltme)