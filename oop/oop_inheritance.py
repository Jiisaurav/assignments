class Vehicle:
    def __init__(self,model,color,no_of_wheels):
        self.model = model
        self.color = color
        self.no_of_wheels = no_of_wheels
        
    def __str__(self):
        return (f"{self.model}, {self.color}, {self.no_of_wheels}")
        
    def horn(self):
        print("Peep Peep Peep")
        
class Car(Vehicle):
    def __init__(self,model,color,no_of_wheels,no_passenger):
        super().__init__(model,color,no_of_wheels)
        self.no_passenger = no_passenger
        
    
    def __str__(self):
        return (f"{self.model}, {self.color}, {self.no_of_wheels}, {self.no_passenger}")
    
    
    def add_passanger(self,apnd):
        print(f"Number of passengers present {self.no_passenger}")
        print(f"New passenger in que {apnd}")
        new_passenger = self.no_passenger + apnd
        
        if new_passenger > 5:
            left_out = new_passenger -5
            print(f"5 passengers only sorry cant take {left_out} people")
        else:
            print(f"Total Passenger {new_passenger}")
    
            
class Bus(Vehicle):
    def __init__(self,model,color,no_of_wheels,no_passenger):
        super().__init__(model,color,no_of_wheels)
        self.no_passenger = no_passenger
        
    def __str__(self,):
        return (f"{self.model}, {self.color}, {self.no_of_wheels}, {self.no_passenger}")
    
    
    def add_passanger(self,apnd):
        print(f"Number of passengers present {self.no_passenger}")
        print(f"New passenger in que {apnd}")
        new_passenger = self.no_passenger + apnd
        
        if new_passenger > 20:
            left_out = new_passenger -20
            print(f"20 passengers only sorry {left_out} passengers wait for another bus")
        else:
            print(f"Total Passengers {new_passenger}")
                    
vehicle = Vehicle("Ford", "red", 4)
print(vehicle)
vehicle.horn()

print("Car")
car = Car("audi", "black", 4, 4)
print(car)
car.add_passanger(3)

print("Bus")
bus = Bus("Force", "yellow", 4, 10)
print(bus)
bus.add_passanger(15)